//
//  CategoryTableViewController.swift
//  Todoey
//
//  Created by warbo on 22/10/2021.
//  Copyright © 2021 App Brewery. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework
class CategoryTableViewController: SwipeTableViewController {
    let realm = try! Realm()
    var cateGoryArray: Results<Category>?
   
 
    override func viewDidLoad() {
        super.viewDidLoad()
        print( FileManager.default.urls(for: .documentDirectory ,in: .userDomainMask))
        loadCategory()
        tableView.rowHeight = 80
        tableView.separatorStyle = .none
    }
    override func viewWillAppear(_ animated: Bool) {
        guard let navBar = navigationController?.navigationBar else {
            fatalError("NavigationController does not exist")
        }
        navBar.backgroundColor = UIColor(hexString: "1D9BF6")
    }
    // MARK: -Table View Data Source Method


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cateGoryArray?.count ?? 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if let category = cateGoryArray?[indexPath.row]{
            cell.textLabel?.text = category.name ?? "No Value added yet"
            guard let cateGoryColour = UIColor(hexString: category.categoryColor)else{fatalError()}
            cell.backgroundColor = UIColor(hexString: cateGoryArray?[indexPath.row].categoryColor ?? "1D9F6")
            cell.textLabel?.textColor = ContrastColorOf(cateGoryColour, returnFlat: true)
        }
       
        
        
       
      
       return cell
    }
    // MARK: - Table View Delegate Method
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     performSegue(withIdentifier: "goToItems", sender: self)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! TodoListViewController
        if let indexPath = tableView.indexPathForSelectedRow{
            destinationVC.selectedCategory = cateGoryArray?[indexPath.row]
        }
    }
    
    
    // MARK: - Add New Categories
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        let alert = UIAlertController(title: "Add New Todoey Category", message: "", preferredStyle:.alert )
        let action = UIAlertAction(title: "Add Category", style: .default) { [self] (action) in
            let newCategory = Category()
            newCategory.name = textField.text!
            newCategory.categoryColor = UIColor.randomFlat().hexValue()
            save(category: newCategory)
        }
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create New Category"
          
            textField = alertTextField
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
      
    }
    // MARK: - Data Manipulation Method
    
    func save(category: Category){
        do{
            try realm.write{
                realm.add(category)
            }
        }catch{
            print("Error to saving context\(error)")
        }
        self.tableView.reloadData()
            
    }
    func loadCategory(){
        cateGoryArray = realm.objects(Category.self)
        tableView.reloadData()
    }
    // MARK: -Delete CateGory Method
    override func updateModel(at indexPath: IndexPath) {
        if let categoryDeletetion = cateGoryArray?[indexPath.row]{
            do{
                try realm.write {
                    realm.delete(categoryDeletetion)
                }
            }catch{
                print("error deleting category\(error)")
            }
         
        }
    }
}
// MARK: -Swipe Cell Delegate Method

//
//  ViewController.swift
//  Todoey
//
//  Created by Philipp Muellauer on 02/12/2019.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework
class TodoListViewController: SwipeTableViewController {
    var todoeyItems: Results<Item>?
    let realm = try! Realm()
    
    @IBOutlet weak var searchBar: UISearchBar!
    var selectedCategory: Category?{
        didSet{
            loadItems()
        }
        
    }
   
 
    override func viewDidLoad() {
        print( FileManager.default.urls(for: .documentDirectory ,in: .userDomainMask))
        super.viewDidLoad()
        
        tableView.rowHeight = 80
        print(Date())
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if let colourHex = selectedCategory?.categoryColor{
            title = selectedCategory!.name
            guard let navBar = navigationController?.navigationBar else {
                fatalError("NavigationController does not exist")
            }
            if let  navBarColour = UIColor(hexString: colourHex) {
                navBar.barTintColor = UIColor(hexString: colourHex)
                navBar.tintColor = ContrastColorOf(navBarColour, returnFlat: true)
//                searchBar.barTintColor = UIColor(hexString: colourHex)
                navBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: ContrastColorOf(navBarColour, returnFlat: true)]
            }
            
        }
        
    }
    
    // MARK: -Table View Data Source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoeyItems?.count ?? 1
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = todoeyItems?[indexPath.row]{
            do{
                        try realm.write {
                            item.done = !item.done
                        }

            }catch{
                print("error saving status\(error)")
            }
            self.tableView.reloadData()
    }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if let item = todoeyItems?[indexPath.row]{
            cell.textLabel?.text = item.title
            if let colour = UIColor(hexString: selectedCategory!.categoryColor)?.darken(byPercentage: CGFloat(indexPath.row)/CGFloat(todoeyItems!.count)){
                cell.backgroundColor = colour
                cell.textLabel?.textColor = ContrastColorOf(colour, returnFlat: true)
            }
           
            cell.accessoryType = item.done ? .checkmark : .none
        }else{
            print("No Items Added Yet")
        }
     
        return cell
    }
    // MARK: -Table View Delegate Methods
    override func updateModel(at indexPath: IndexPath) {
        if let item = todoeyItems?[indexPath.row] {
            do{
                try realm.write {
                   
                    realm.delete(item)
                }
            }catch{
                print("error saving done status \(error) ")
            }
          
            
        }
      
    }
        
       
    // MARK: - Add New Items
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        let alert = UIAlertController(title: "Add new Todey Item", message: "", preferredStyle:.alert)
        let action = UIAlertAction(title: "Add Item", style: .default) { [self] (action) in
            if let currentCategory = self.selectedCategory {
                do{
                    try self.realm.write {
                         let newItem = Item()
                         newItem.title = textField.text!
                        newItem.dateCreated = Date()
                    
                        currentCategory.items.append(newItem)
                     }
                }catch{
                    print("error saving items \(error)")
                }
             
            }
            self.tableView.reloadData()

        }
        alert.addTextField {(alertTextField) in
            alertTextField.placeholder = "Create new Item"
            textField = alertTextField
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
       
    }
    // MARK: -Model Manupulation Method
   
    func loadItems (){
        todoeyItems = selectedCategory?.items.sorted(byKeyPath: "title", ascending: true)
        tableView.reloadData()
    }

}

// MARK: -Search Bar Method
extension TodoListViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        todoeyItems = todoeyItems?.filter("title CONTAINS[cd] %@",searchBar.text!).sorted(byKeyPath: "dateCreated",ascending: true)
        print(searchBar.text!)
        tableView.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadItems()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
 

}
